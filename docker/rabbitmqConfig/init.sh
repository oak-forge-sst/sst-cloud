#!/bin/bash

(sleep 30 ; \
rabbitmqctl add_user "$RABBITMQ_USER" "$RABBITMQ_PASSWORD" ; \
rabbitmqctl set_user_tags "$RABBITMQ_USER" administrator ; \
rabbitmqctl set_permissions -p / "$RABBITMQ_USER" ".*" ".*" ".*" ; \
echo "*** User '$RABBITMQ_USER' with password '$RABBITMQ_PASSWORD' created. ***" ; \
rabbitmqctl delete_user guest ; \
echo "*** Unused user 'guest' deleted ***") &
# shellcheck disable=SC2068
rabbitmq-server $@
