#### Java Code Conventions
We are using code conventions from Oracle - [link](https://www.oracle.com/technetwork/java/codeconventions-150003.pdf)

#### Configuring master pass file location 
export SPRING_CONFIG_ADDITIONALLOCATION=pass.yml
