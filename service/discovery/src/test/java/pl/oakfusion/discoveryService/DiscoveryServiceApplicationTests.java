package pl.oakfusion.discoveryService;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.*;

@SpringBootTest
@TestPropertySource(properties = {
        "CERTIFICATE_PASSWORD=123456",
})
@ActiveProfiles("test")
class DiscoveryServiceApplicationTests {
    /**
     *
     *  To be developed in the future
     *  https://gitlab.com/oak-forge-sst/sst-cloud/-/issues/20
     */
    /*
	@Test
	void contextLoads() {
	}
    */
}
