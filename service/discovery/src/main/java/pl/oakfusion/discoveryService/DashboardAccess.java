package pl.oakfusion.discoveryService;

import org.apache.catalina.connector.Connector;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.*;

//TODO Access to Eureka dashboard without SSL only for testing.
@Configuration
class DashboardAccess {

    @Bean
    public ServletWebServerFactory tomcatEmbeddedServletContainerFactory() {
        final TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory ();
        factory.addAdditionalTomcatConnectors(this.createConnection());
        return factory;
    }

    private Connector createConnection() {
        final String protocol = "org.apache.coyote.http11.Http11NioProtocol";
        final Connector connector = new Connector(protocol);

        connector.setScheme("http");
        connector.setPort(8762);
        connector.setRedirectPort(8761);
        return connector;
    }
}
