package pl.oakfusion.gateway;
/*
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;
import pl.oakfusion.idm_commands.IdmSecurityEndpointsHandler;
import pl.oakfusion.idm_commands.Roles;


@Configuration
@EnableWebSecurity
@EnableConfigurationProperties({SecurityConstants.class})
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    SecurityConstants securityConstants;
    @Autowired
    Roles roles;

    @Bean
    @ConfigurationProperties(prefix = "idm.roles")
    public Roles roles() {
        return new Roles();
    }

    @Value("${blacklist-jwt-lookup.uri}")
    private String blacklistJWTURI;

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .cors()
                .and()
                .csrf().disable()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .requestMatchers(IdmSecurityEndpointsHandler.getPermitAllIdmMatches().toArray(new RequestMatcher[0]))
                .permitAll()
                .requestMatchers(IdmSecurityEndpointsHandler.getAdminRolesIdmMatches().toArray(new RequestMatcher[0]))
                .hasRole(roles.getAdminRole())
                .antMatchers(".admin.")
                .hasRole(roles.getAdminRole())
                .antMatchers("/login")
                .permitAll()
                .antMatchers("/idm/register")
                .permitAll()
                .anyRequest()
                .authenticated()
                .and()

                .addFilterBefore(new JwtAuthorizationFilter(authenticationManager(), securityConstants, blacklistJWTURI), UsernamePasswordAuthenticationFilter.class);
    }

}
*/