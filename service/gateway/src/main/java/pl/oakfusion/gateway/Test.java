package pl.oakfusion.gateway;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
public class Test {

    private String message = "test message";

    @RequestMapping("/test")
    String getMessage() {
        return this.message;
    }

}