package pl.oakfusion.gateway;
/*
import io.jsonwebtoken.*;
import org.slf4j.*;
import org.springframework.security.authentication.*;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.net.URI;
import java.net.http.*;
import java.util.List;
import java.util.stream.Collectors;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {
    private static final String TOKEN_HEADER = "idmToken";
    private static final Logger LOG = LoggerFactory.getLogger(JwtAuthorizationFilter.class);

    private final SecurityConstants securityConstants;
    private final String blacklistJWTURI;

    public JwtAuthorizationFilter(AuthenticationManager authenticationManager, SecurityConstants securityConstants, String blacklistJWTURI) {
        super(authenticationManager);
        this.securityConstants = securityConstants;
        this.blacklistJWTURI = blacklistJWTURI;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws IOException, ServletException {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(TOKEN_HEADER)) {
                    try {
                        // build request and check if token is blacklisted
                        HttpRequest requestBlacklistJWT = HttpRequest
                                .newBuilder()
                                .uri(URI.create(blacklistJWTURI))
                                .header("token", cookie.getValue())
                                .build();

                        HttpClient httpClient = HttpClient.newHttpClient();

                        HttpResponse<String> responseBlacklistJWT =
                                httpClient.send(requestBlacklistJWT, HttpResponse.BodyHandlers.ofString());

                        boolean isTokenBlacklisted = Boolean.parseBoolean(responseBlacklistJWT.body());

                        if (isTokenBlacklisted) throw new BlacklistException(ErrorCode.TOKEN_HAS_BEEN_BLACKLISTED);

                        // parse token and set authentication
                        byte[] signingKey = securityConstants.getKey().getBytes();

                        Jws<Claims> parsedToken = Jwts.parser()
                                .setSigningKey(signingKey)
                                .parseClaimsJws(cookie.getValue());

                        String username = parsedToken
                                .getBody()
                                .getSubject();

                        List<SimpleGrantedAuthority> roles = ((List<?>) parsedToken
                                .getBody()
                                .get("rol"))
                                .stream()
                                .map(authority -> new SimpleGrantedAuthority((String) authority))
                                .collect(Collectors.toList());

                        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(username, null, roles);
                        SecurityContextHolder.getContext().setAuthentication(authentication);
                    } catch (InterruptedException e) {
                        LOG.error("Encountered problem with connection");
                        e.printStackTrace();
                    } catch (JwtException | IllegalArgumentException e) {
                        LOG.error("Encountered problem with token");
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        filterChain.doFilter(request, response);
    }
}
*/