package pl.oakfusion.core;

import org.apache.http.impl.client.*;
import org.apache.http.ssl.SSLContexts;
import org.springframework.beans.factory.annotation.*;
import org.springframework.cloud.config.client.*;
import org.springframework.context.annotation.*;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;

@Configuration
public class SslConfig {

    @Value("${CERTIFICATE_PASSWORD}")
    private String certPassword;

    @Autowired
    ConfigClientProperties properties;

    @Primary
    @Bean
    public ConfigServicePropertySourceLocator configServicePropertySourceLocator() throws Exception {
        final char[] password = certPassword.toCharArray();
        final ClassPathResource resourceFile = new ClassPathResource("core.p12");

        SSLContext sslContext = SSLContexts.custom()
                .loadKeyMaterial(resourceFile.getURL(), password, password)
                .loadTrustMaterial(resourceFile.getURL(), password).build();
        CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLContext(sslContext)
                .setSSLHostnameVerifier((s, sslSession) -> true)
                .build();
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
        ConfigServicePropertySourceLocator configServicePropertySourceLocator = new ConfigServicePropertySourceLocator(properties);
        configServicePropertySourceLocator.setRestTemplate(new RestTemplate(requestFactory));
        return configServicePropertySourceLocator;
    }
}
