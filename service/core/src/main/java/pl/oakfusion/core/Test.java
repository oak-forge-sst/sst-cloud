package pl.oakfusion.core;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

@RefreshScope
@RestController
public class Test {

    @Value("${message:Hello default}")
    private String message;

    @RequestMapping("/test")
    String getMessage() {
        return this.message;
    }

}